package my.app.groceryplanning;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class AddItem extends BaseActivity {

    public Button btnAdd;
    public EditText etItemName,etItemPrice,etItemQuantity;
    public TextView tvListName, tvListBudget, tvMainCategory, tvSubCategory;
    public String sItemName,sItemPrice,sItemQuantity;
    private ShoppingList shoppingList;
    private Item item;
    private Boolean error = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        btnAdd = (Button)findViewById(R.id.btnAdd);
        etItemName = (EditText)findViewById(R.id.etItemName);
        etItemPrice = (EditText)findViewById(R.id.etItemPrice);
        etItemQuantity = (EditText)findViewById(R.id.etItemQuantity);
        tvListName = (TextView)findViewById(R.id.tvListName);
        tvListBudget = (TextView)findViewById(R.id.tvListBudget);
        tvMainCategory  = (TextView)findViewById(R.id.tvMainCategory);
        tvSubCategory = (TextView)findViewById(R.id.tvSubCategory);


        shoppingList = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGLIST);
        item = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGITEM);

        tvListName.setText(shoppingList.getName());
        tvListBudget.setText("RM " + shoppingList.getBudget());
        tvMainCategory.setText(item.getMainCategory());
        tvSubCategory.setText(item.getSubCategory());

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sItemName = etItemName.getText().toString();
                sItemPrice = etItemPrice.getText().toString();
                sItemQuantity = etItemQuantity.getText().toString();

                if(!sItemName.isEmpty()
                        && !sItemPrice.isEmpty()
                        && !sItemQuantity.isEmpty()){

                    try
                    {
                        Float.parseFloat(sItemPrice);
                        Integer.parseInt(sItemQuantity);
                    }
                    catch (NumberFormatException nfe)
                    {
                        Toast.makeText(getApplicationContext(), "Invalid Price/Quantity", Toast.LENGTH_SHORT).show();
                        error = true;
                    }

                    System.out.println(error);
                    Log.d("name",sItemName);
                    Log.d("price",sItemPrice);
                    Log.d("quantity",sItemQuantity);

                    //if no error
                    if(!error){

                        Float currentTotal = shoppingList.getTotalItemCost() + ( Float.parseFloat(sItemQuantity) * Float.parseFloat(sItemPrice)) ;
                        if(currentTotal > Float.parseFloat(shoppingList.getBudget())){
                            Toast.makeText(getApplicationContext(),"Overbudget. Balance is " + shoppingList.getBudgetBalance(),Toast.LENGTH_LONG).show();
                        }else{
                            //set new items
                            item.setName(sItemName);
                            item.setPrice(sItemPrice);
                            item.setQuantity(sItemQuantity);
                            //get current items from shopping list
                            List<Item> shoppingListItems = shoppingList.getItems();
                            //add items to currentList
                            shoppingListItems.add(item);
                            //reset back items in shopping list
                            shoppingList.setItems(shoppingListItems);
                            //lets go to last page
                            Intent i = new Intent(AddItem.this, ShoppingListItems.class);
                            i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST,shoppingList);
                            startActivity(i);
                        }
                    }
                }
            }
        });

    }

}
