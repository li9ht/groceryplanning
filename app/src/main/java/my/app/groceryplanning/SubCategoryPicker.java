package my.app.groceryplanning;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import my.app.groceryplanning.adapter.SubCategoryAdapter;
import my.app.groceryplanning.database.DbSubCategory;
import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class SubCategoryPicker extends BaseActivity {

    public ListView lvSubCat;
    public TextView tvListName,tvListBudget,tvCategoryName;
    public Button btnAdd;
    public String listName,listBudget, mainCategoryId,mainCategoryName;
    public String[] aCategoryItems;
    private ShoppingList shoppingList;
    private Item item;
    private ArrayList<String> catItems;
    private Integer mainCategoryfilteredId;
    private SubCategoryAdapter listAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_list);

        lvSubCat = (ListView)findViewById(R.id.lvSubCat);
        tvListBudget = (TextView)findViewById(R.id.tvListBudget);
        tvListName = (TextView)findViewById(R.id.tvListName);
        tvCategoryName = (TextView) findViewById(R.id.tvListName);
        btnAdd = (Button) findViewById(R.id.btnAdd);

        //get extra shoppinglist & items
        shoppingList = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGLIST);
        item = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGITEM);

        //get values
        listName = shoppingList.getName();
        listBudget = shoppingList.getBudget();
        mainCategoryId = item.getMainCategoryId();
        mainCategoryfilteredId = Integer.parseInt(mainCategoryId.replaceAll("[^0-9]", ""));

        //get category name from tag
        int resId = this.getResources().getIdentifier(mainCategoryId, "string", this.getPackageName());
        mainCategoryName = getString(resId);

        // set ui values
        tvListName.setText(listName);
        tvListBudget.setText("RM" +listBudget);
        tvCategoryName.setText(mainCategoryName);

        //set listview
        listAdapter = new SubCategoryAdapter(this,0);
        List<DbSubCategory> subCategories = DbSubCategory.getWhereCategory(mainCategoryfilteredId);
        if(subCategories.size() > 0){
            listAdapter.addAll(subCategories);
        }
        listAdapter.setShoppingList(shoppingList);
        lvSubCat.setAdapter(listAdapter);
    }
}
