package my.app.groceryplanning;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import my.app.groceryplanning.adapter.SubCategoryItemListAdapter;
import my.app.groceryplanning.database.DbCategoryItems;
import my.app.groceryplanning.database.DbSubCategory;
import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class SubCategoryItemPicker extends BaseActivity{

    public ListView lvSubCat;
    public TextView tvListName,tvListBudget,tvCategoryName;
    public Button btnAdd,btnNewItem;
    public String listName,listBudget, mainCategoryName,ssubCategoryId;
    private ShoppingList shoppingList;
    private Item item;
    private Integer subCategoryId;
    private SubCategoryItemListAdapter listAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category_picker);

        lvSubCat = (ListView)findViewById(R.id.lvSubCat);
        tvListBudget = (TextView)findViewById(R.id.tvListBudget);
        tvListName = (TextView)findViewById(R.id.tvListName);
        tvCategoryName = (TextView) findViewById(R.id.tvListName);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnNewItem = (Button)findViewById(R.id.btnNewItem);


        //get extra shoppinglist & items
        shoppingList = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGLIST);
        item = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGITEM);
        ssubCategoryId = getIntent().getStringExtra(GroceryPlanning.EXTRA_SUBCATEGORY);
        subCategoryId = Integer.parseInt(ssubCategoryId);

        //get values
        listName = shoppingList.getName();
        listBudget = shoppingList.getBudget();

        //get category name from tag
        System.out.println(subCategoryId);
        DbSubCategory dbSubCategory = DbSubCategory.getWhereId(subCategoryId);
        mainCategoryName = dbSubCategory.name;

        // set ui values
        tvListName.setText(listName);
        tvListBudget.setText("RM" +listBudget);
        tvCategoryName.setText(mainCategoryName);

        //set listview
        listAdapter = new SubCategoryItemListAdapter(this,0,shoppingList);
        List<DbCategoryItems> categoryItems = DbCategoryItems.getWhereCategory(subCategoryId);
        if(categoryItems.size() > 0){
            listAdapter.addAll(categoryItems);
        }

        lvSubCat.setAdapter(listAdapter);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(listAdapter.getDbCategoryItemsList());
                //retian previous list
                List<Item> itemList = shoppingList.getItems();
                itemList.addAll(listAdapter.getItemList());

                shoppingList.setItems(itemList);
                Intent i = new Intent(SubCategoryItemPicker.this,ShoppingListItems.class);
                i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST, shoppingList);
                startActivity(i);
            }
        });

        btnNewItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newItem();
            }
        });

    }

    public void newItem(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.add_new_cat_item, null);
        final EditText etName = (EditText) view.findViewById(R.id.tvName);
        final EditText etPrice = (EditText) view.findViewById(R.id.tvPrice);

        builder.setView(view)
                .setPositiveButton("Create", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        System.out.println(etName.getText());
                        System.out.println(etPrice.getText());
                        DbCategoryItems dbCategoryItems = new DbCategoryItems(etName.getText().toString(),subCategoryId.toString(),etPrice.getText().toString());
                        dbCategoryItems.save();

                        listAdapter.clear();
                        List<DbCategoryItems> categoryItems = DbCategoryItems.getWhereCategory(subCategoryId);
                        if(categoryItems.size() > 0){
                            listAdapter.addAll(categoryItems);
                        }
                        listAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog =  builder.create();
        dialog.show();
    }


    public void editItem(View v){
        final DbCategoryItems categoryItems = (DbCategoryItems) v.getTag();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.add_new_cat_item, null);
        final EditText etName = (EditText) view.findViewById(R.id.tvName);
        final EditText etPrice = (EditText) view.findViewById(R.id.tvPrice);
        etName.setText(categoryItems.name);
        etPrice.setText(categoryItems.price);

        builder.setView(view)
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        DbCategoryItems dbCategoryItems1 = DbCategoryItems.getWhereId(categoryItems.itemId);
                        dbCategoryItems1.name = etName.getText().toString();
                        dbCategoryItems1.price = etPrice.getText().toString();
                        dbCategoryItems1.save();

                        listAdapter.clear();
                        List<DbCategoryItems> categoryItems = DbCategoryItems.getWhereCategory(subCategoryId);
                        if(categoryItems.size() > 0){
                            listAdapter.addAll(categoryItems);
                        }
                        listAdapter.notifyDataSetChanged();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        Dialog dialog =  builder.create();
        dialog.show();
    }
}
