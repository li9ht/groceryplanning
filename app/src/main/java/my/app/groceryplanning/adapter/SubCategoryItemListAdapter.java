package my.app.groceryplanning.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import my.app.groceryplanning.R;
import my.app.groceryplanning.database.DbCategoryItems;
import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;

public class SubCategoryItemListAdapter extends ArrayAdapter<DbCategoryItems> {

    private final Context context;
    ViewHolder viewHolder;
    List<DbCategoryItems> dbCategoryItemsList ;
    List<Item> itemList = new ArrayList<Item>();
    ShoppingList shoppingList;

    public SubCategoryItemListAdapter(Context c, int resource,ShoppingList shoppingList) {
        super(c, resource);
        context = c;
        this.shoppingList = shoppingList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        dbCategoryItemsList = new ArrayList<DbCategoryItems>();
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.category_item, null);

            viewHolder = new ViewHolder();
            viewHolder.cbItem = (CheckBox) rowView.findViewById(R.id.cbItem);
            viewHolder.tvPrice = (TextView) rowView.findViewById(R.id.tvPrice);
            viewHolder.spQuantity = (Spinner) rowView.findViewById(R.id.spQuantity);
            viewHolder.btnEditItem = (Button) rowView.findViewById(R.id.btnEditItem);

            //set checkbox actions
            viewHolder.cbItem.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    View view = (ViewGroup) v.getParent();
                    CheckBox cb = (CheckBox)  view.findViewById(R.id.cbItem);
                    Spinner spQuantity = (Spinner) view.findViewById(R.id.spQuantity);
                    Button btnEditItem = (Button) view.findViewById(R.id.btnEditItem);
                    //get item
                    DbCategoryItems item = (DbCategoryItems) cb.getTag();
                    if(!overBudget(Double.parseDouble(item.price))){
                       if(cb.isChecked()){
                            spQuantity.setEnabled(false);
                            btnEditItem.setEnabled(false);
                            //add item
                            dbCategoryItemsList.add(item);
                            Item item1 = new Item();
                            item1.setName(item.name);
                            item1.setQuantity(spQuantity.getSelectedItem().toString());
                            item1.setMainCategoryId("1");
                            item1.setMainCategory("maincat");
                            item1.setSubCategory(item.category);
                            item1.setPrice(item.price);
                            itemList.add(item1);
                        }else{
                            spQuantity.setEnabled(true);
                            btnEditItem.setEnabled(true);
                            //remove
                            for (Item i : itemList){
                                if(i.getName() == item.name){
                                    itemList.remove(i);
                                }
                            }
                        }

                        System.out.println(itemList);
                    }else {
                        cb.setChecked(false);
                    }
                }
            });

            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        DbCategoryItems categoryItems = getItem(position);
        if (categoryItems != null) {
            viewHolder.cbItem.setText(categoryItems.name.toString());
            viewHolder.cbItem.setTag(categoryItems);
            viewHolder.tvPrice.setText(categoryItems.price.toString());
            viewHolder.btnEditItem.setTag(categoryItems);
        }

        return rowView;
    }

    //get all checked items
    public List<DbCategoryItems> getDbCategoryItemsList(){
        return dbCategoryItemsList;
    }

    public List<Item> getItemList (){
        return itemList;
    }

    public Double getTotalItem(){
        Double total = 0.0;
        for(Item i:this.itemList){
            total = total + Double.parseDouble(i.getPrice());

        }

        return total;
    }

    public boolean overBudget(Double itemprice){
        if(( getTotalItem() + itemprice ) > shoppingList.getBudgetBalance()){
            Double balance =  shoppingList.getBudgetBalance() - getTotalItem();
            Toast.makeText(context,"Overbudget .Balance is " + Double.toString(balance) ,Toast.LENGTH_LONG ).show();
            return true;
        }
        else {
            return false;
        }
    }

    static class ViewHolder {
        CheckBox cbItem;
        TextView tvPrice;
        Spinner spQuantity;
        Button btnEditItem;
    }
}

