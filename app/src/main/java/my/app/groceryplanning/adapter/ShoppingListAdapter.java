package my.app.groceryplanning.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import my.app.groceryplanning.GroceryPlanning;
import my.app.groceryplanning.R;
import my.app.groceryplanning.ShoppingListItems;
import my.app.groceryplanning.database.DbItem;
import my.app.groceryplanning.database.DbShoppingList;
import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class ShoppingListAdapter extends ArrayAdapter<DbShoppingList> {

    private final Context context;
    ViewHolder viewHolder;

    public ShoppingListAdapter(Context c, int resource) {
        super(c, resource);
        context = c;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.shopping_list_item, null);

            viewHolder = new ViewHolder();
            viewHolder.tvTitle = (TextView) rowView.findViewById(R.id.tvListTitle);
            rowView.setTag(viewHolder);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String listId = v.findViewById(R.id.tvListTitle).getTag().toString();

                    List<DbShoppingList> dbShoppingList = DbShoppingList.whereId(Integer.parseInt(listId));
                    DbShoppingList shoppingListDetail = dbShoppingList.get(0);

                    List<DbItem> dbItemList = DbItem.getWhereListId(Integer.parseInt(shoppingListDetail.getId().toString()));

                    ShoppingList shoppingList = new ShoppingList();
                    shoppingList.setName(shoppingListDetail.listName);
                    shoppingList.setBudget(Integer.toString(shoppingListDetail.listBudget));
                    shoppingList.setId(Integer.parseInt(shoppingListDetail.getId().toString()));

                    List<Item> itemList = new ArrayList<Item>();
                    for (DbItem dbi:dbItemList){
                        Item tempItem = new Item();
                        tempItem.setQuantity(dbi.quantity);
                        tempItem.setSubCategory(dbi.subCategory);
                        tempItem.setMainCategory(dbi.mainCategory);
                        tempItem.setName(dbi.name);
                        tempItem.setPrice(dbi.price);
                        tempItem.setMainCategoryId(dbi.mainCategoryId);
                        itemList.add(tempItem);
                    }

                    shoppingList.setItems(itemList);

                    Intent i = new Intent(context, ShoppingListItems.class);
                    i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST,shoppingList);
                    context.startActivity(i);
                }
            });

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        DbShoppingList shoppingList = getItem(position);
        if (shoppingList != null) {
            viewHolder.tvTitle.setText(shoppingList.listName);
            viewHolder.tvTitle.setTag(shoppingList.getId().toString());
            System.out.println(shoppingList.getId().toString());
        }

        return rowView;
    }

    static class ViewHolder {
        TextView tvTitle;
    }
}
