package my.app.groceryplanning.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import my.app.groceryplanning.GroceryPlanning;
import my.app.groceryplanning.R;
import my.app.groceryplanning.SubCategoryItemPicker;
import my.app.groceryplanning.database.DbSubCategory;
import my.app.groceryplanning.models.ShoppingList;

public class SubCategoryAdapter extends ArrayAdapter<DbSubCategory> {

    private final Context context;
    ViewHolder viewHolder;
    ShoppingList shoppingList;

    public SubCategoryAdapter(Context c, int resource) {
        super(c, resource);
        context = c;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.sub_category_list_item, null);

            viewHolder = new ViewHolder();
            viewHolder.tvName = (TextView) rowView.findViewById(R.id.tvName);
            rowView.setTag(viewHolder);

            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DbSubCategory subCategory = (DbSubCategory) v.findViewById(R.id.tvName).getTag();
                    Intent i = new Intent(context, SubCategoryItemPicker.class);
                    i.putExtra(GroceryPlanning.EXTRA_SUBCATEGORY,subCategory.id.toString());
                    i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST,shoppingList);
                    context.startActivity(i);
                }
            });

        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }

        DbSubCategory dbSubCategory = getItem(position);
        if (dbSubCategory != null) {
            viewHolder.tvName.setText(dbSubCategory.name.toString());
            viewHolder.tvName.setTag(dbSubCategory);
        }

        return rowView;
    }

    public void setShoppingList(ShoppingList shoppingList){
        this.shoppingList = shoppingList;
    }

    static class ViewHolder {
        TextView tvName;
    }
}