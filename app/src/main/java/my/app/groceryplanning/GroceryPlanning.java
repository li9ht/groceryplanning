package my.app.groceryplanning;
import android.app.Application;
import com.activeandroid.ActiveAndroid;

public class GroceryPlanning extends Application {

    //use Constant so we dont make silly mistake
    public static final String EXTRA_SHOPPINGLIST = "shoppinglist";
    public static final String EXTRA_SHOPPINGITEM = "shoppingitem";
    public static final String EXTRA_SUBCATEGORY = "subcategory";

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
