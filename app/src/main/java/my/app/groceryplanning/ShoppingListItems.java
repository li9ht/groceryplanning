package my.app.groceryplanning;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import my.app.groceryplanning.database.DbItem;
import my.app.groceryplanning.database.DbShoppingList;
import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class ShoppingListItems extends BaseActivity {

    public ListView lvItems;
    private ShoppingItemsAdapter itemsAdapter;
    public View footerView;
    public Button btnAddItem,btnDelete,btnSaveList;
    public TextView tvListName,tvListBudget,tvTotalItemPrice;
    private ShoppingList shoppingList;
    private Item item;
    private List<Item> itemList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_items);

        lvItems = (ListView)findViewById(R.id.lvItems);
        tvListName = (TextView)findViewById(R.id.tvListName);
        tvListBudget = (TextView)findViewById(R.id.tvListBudget);

        shoppingList = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGLIST);
        //item = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGITEM);

        tvListName.setText(shoppingList.getName());
        tvListBudget.setText("RM " + shoppingList.getBudget());

        itemsAdapter = new ShoppingItemsAdapter(this);
        itemList = shoppingList.getItems();
        for (Item i:itemList){
            itemsAdapter.addItem(i.getName(),i.getQuantity() + " x RM " + i.getPrice());
        }


//        itemsAdapter.addSectionHeaderItem("Vegetables");
//        itemsAdapter.addItem("Item 1");

        lvItems.setAdapter(itemsAdapter);
        footerView =  ((LayoutInflater)getSystemService(LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.shopping_list_footer, null, false);

        //set total price
        tvTotalItemPrice = (TextView)footerView.findViewById(R.id.tvTotalPrice);
        DecimalFormat df = new DecimalFormat("#.##"); //format output
        String sItemsPrice = "RM " + Double.valueOf(df.format(shoppingList.getTotalItemCost()));
        tvTotalItemPrice.setText(sItemsPrice);

        btnAddItem = (Button)footerView.findViewById(R.id.btnAdd);
        btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShoppingListItems.this,MainCategoryPicker.class);
                i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST,shoppingList);
                startActivity(i);
            }
        });

        btnDelete = (Button)footerView.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmDeleteDialog();
            }
        });


        btnSaveList = (Button)footerView.findViewById(R.id.btnSaveShoppingList);
        btnSaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveList();
                Intent i  = new Intent(ShoppingListItems.this,ShoppingLists.class);
                startActivity(i);
            }
        });

        lvItems.addFooterView(footerView);
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void confirmDeleteDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Confirm Delete");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if(shoppingList.getId() > 0){
                    List<DbShoppingList> shoppingListId = DbShoppingList.whereId(shoppingList.getId());
                    if(shoppingListId.size() > 0){
                        DbShoppingList dbShoppingList = shoppingListId.get(0);
                        dbShoppingList.delete();
                    }
                }

                Intent i  = new Intent(ShoppingListItems.this,ShoppingLists.class);
                startActivity(i);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void saveList(){

        Integer listBudget = Integer.parseInt(shoppingList.getBudget());
        String listName = shoppingList.getName();

        ActiveAndroid.beginTransaction();
        try {

            DbShoppingList dbShoppingList = new DbShoppingList();
            dbShoppingList.listBudget = listBudget;
            dbShoppingList.listName = listName;
            dbShoppingList.save();
            Integer listId = Integer.parseInt(dbShoppingList.getId().toString());
            System.out.println(listId);

            for (Item i:itemList) {
                DbItem dbItem = new DbItem();
                dbItem.name =  i.getName();
                dbItem.mainCategory = i.getMainCategory();
                dbItem.mainCategoryId = i.getMainCategoryId();
                dbItem.price = i.getPrice();
                dbItem.quantity = i.getQuantity();
                dbItem.subCategory = i.getSubCategory();
                dbItem.listId = listId;
                dbItem.save();
                Log.d("save",i.getName());
                System.out.println(i);
            }
            ActiveAndroid.setTransactionSuccessful();
        }
        finally {
            ActiveAndroid.endTransaction();
        }




    }
}
