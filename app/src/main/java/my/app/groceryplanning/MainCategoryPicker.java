package my.app.groceryplanning;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import my.app.groceryplanning.models.Item;
import my.app.groceryplanning.models.ShoppingList;


public class MainCategoryPicker extends BaseActivity {

    public ImageView ivCategory1,ivCategory2,ivCategory3,ivCategory4;
    public TextView tvListName,tvListBudget;
    public String listName,listBudget;
    private ShoppingList shoppingList;
    private Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_category_picker);

        ivCategory1 = (ImageView) findViewById(R.id.ivCategory1);
        ivCategory2 = (ImageView) findViewById(R.id.ivCategory2);
        ivCategory3 = (ImageView) findViewById(R.id.ivCategory3);
        ivCategory4 = (ImageView) findViewById(R.id.ivCategory4);
        tvListName = (TextView) findViewById(R.id.tvListName);
        tvListBudget = (TextView)findViewById(R.id.tvListBudget);

        ivCategory1.setTag("main_category_1");
        ivCategory2.setTag("main_category_2");
        ivCategory3.setTag("main_category_3");
        ivCategory4.setTag("main_category_4");

        //get shopping list
        shoppingList = getIntent().getParcelableExtra(GroceryPlanning.EXTRA_SHOPPINGLIST);
        //create new item
        item = new Item();

        //set value from shopping list
        listName = shoppingList.getName();
        listBudget = shoppingList.getBudget();

        tvListName.setText(listName);
        tvListBudget.setText("RM " +listBudget);

        View.OnClickListener btnListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //get item category and set
                String sCategory = v.getTag().toString();
                item.setMainCategoryId(sCategory);
                //send to new page
                Intent i = new Intent(MainCategoryPicker.this, SubCategoryPicker.class);
                i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST, shoppingList);
                i.putExtra(GroceryPlanning.EXTRA_SHOPPINGITEM, item);
                startActivity(i);
            }
        };


        ivCategory1.setOnClickListener(btnListener);
        ivCategory2.setOnClickListener(btnListener);
        ivCategory3.setOnClickListener(btnListener);
        ivCategory4.setOnClickListener(btnListener);

    }
}
