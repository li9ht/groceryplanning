package my.app.groceryplanning;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import my.app.groceryplanning.models.ShoppingList;


public class CreateList extends BaseActivity {

    public Button btnCreate , btnCancel;
    public EditText etListName, etListBudget;
    public float fBudget = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_list);

        btnCreate = (Button)findViewById(R.id.btnCreateShoppingList);
        btnCancel = (Button)findViewById(R.id.btnCancelShoppingList);
        etListName = (EditText)findViewById(R.id.etListName);
        etListBudget = (EditText)findViewById(R.id.etListBudget);

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String listName = etListName.getText().toString().trim();
                String listBudget = etListBudget.getText().toString().trim();

                if(!listName.isEmpty() && !listBudget.isEmpty()){

                    // to validate budget is number
                    try
                    {
                        fBudget = Float.parseFloat(listBudget);
                    }
                    catch (NumberFormatException nfe)
                    {
                        Toast.makeText(getApplicationContext(),"Invalid Budget",Toast.LENGTH_SHORT).show();
                    }
                    //if valid budget
                    if(fBudget > 0){

                        ShoppingList shoppingList = new ShoppingList();
                        shoppingList.setName(listName);
                        shoppingList.setBudget(listBudget);
                        shoppingList.setId(0);

                        Intent i = new Intent(CreateList.this, MainCategoryPicker.class);
                        i.putExtra(GroceryPlanning.EXTRA_SHOPPINGLIST, shoppingList);
                        startActivity(i);
                    }
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CreateList.this,ShoppingLists.class);
                startActivity(i);
            }
        });
    }



}
