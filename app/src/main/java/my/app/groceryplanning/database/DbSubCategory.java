package my.app.groceryplanning.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "sub_category",id = "_id")
public class DbSubCategory extends Model {

    @Column(name = "_id")
    public Integer id;
    @Column(name = "name")
    public String name;
    @Column(name = "category")
    public String category;

    public DbSubCategory() {
        super();
    }

    public DbSubCategory(String name,
                           String category
    ) {
        super();
        this.name = name;
        this.category = category;
    }


    public static List<DbSubCategory> getWhereCategory(Integer categoryId) {
        return new Select()
                .from(DbSubCategory.class)
                .where("category = ?", categoryId)
                .execute();
    }

    public static DbSubCategory getWhereId(Integer categoryId) {
        return new Select()
                .from(DbSubCategory.class)
                .where("_id = ?", categoryId)
                .executeSingle();
    }
}