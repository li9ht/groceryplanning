package my.app.groceryplanning.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "CategoryItems",id = "id")
public class DbCategoryItems extends Model {

    @Column(name = "id")
    public Integer itemId;
    @Column(name = "name")
    public String name;
    @Column(name = "category")
    public String category;
    @Column(name = "price")
    public String price;

    public DbCategoryItems() {
        super();
    }

    public DbCategoryItems(String name,
                           String category,
                           String price
    ) {
        super();
        this.name = name;
        this.category = category;
        this.price = price;
    }


    public static List<DbCategoryItems> getWhereCategory(Integer categoryId) {
        return new Select()
                .from(DbCategoryItems.class)
                .where("category = ?", categoryId)
                .execute();
    }

    public static DbCategoryItems getWhereId(Integer id) {
        return new Select()
                .from(DbCategoryItems.class)
                .where("id = ?", id)
                .executeSingle();
    }
}

