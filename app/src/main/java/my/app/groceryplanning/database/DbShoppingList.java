package my.app.groceryplanning.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "ShoppingLists")
public class DbShoppingList extends Model {

    @Column(name = "_id")
    public Integer _id;
    @Column(name = "listName")
    public String listName;
    @Column(name = "listBudget")
    public int listBudget;

    public DbShoppingList() {
        super();
    }

    public DbShoppingList(String listName, int listBudget) {
        super();
        this.listName = listName;
        this.listBudget = listBudget;
    }

    public static List<DbShoppingList> getAll() {
        // This is how you execute a query
        return new Select()
                .from(DbShoppingList.class)
                .execute();
    }

    public static List<DbShoppingList> whereId(Integer id) {
        // This is how you execute a query
        return new Select()
                .from(DbShoppingList.class)
                .where("id = ?",id)
                .execute();
    }





}
