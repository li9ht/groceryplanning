package my.app.groceryplanning.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "Items")
public class DbItem extends Model {

    @Column(name = "list_id")
    public Integer listId;
    @Column(name = "name")
    public String name;
    @Column(name = "main_category")
    public String mainCategory;
    @Column(name = "main_categoryId")
    public String mainCategoryId;
    @Column(name = "sub_category")
    public String subCategory;
    @Column(name = "price")
    public String price;
    @Column(name = "quantity")
    public String quantity;

    public DbItem() {
        super();
    }

    public DbItem(String name,
                  String mainCategory,
                  String mainCategoryId,
                  String subCategory,
                  String price,
                  String quantity,
                  int listId
    ) {
        super();
        this.name = name;
        this.mainCategory = mainCategory;
        this.mainCategoryId = mainCategoryId;
        this.subCategory = subCategory;
        this.price = price;
        this.quantity = quantity;
        this.listId = listId;
    }


    public static List<DbItem> getWhereListId(Integer listId) {
        return new Select()
                .from(DbItem.class)
                .where("list_id = ?", listId)
                .execute();
    }
}
