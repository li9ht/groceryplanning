package my.app.groceryplanning.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class ShoppingList implements Parcelable {

   private Integer id;
   private String name;
   private String budget;
   private List<Item> items = new ArrayList<Item>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The budget
     */
    public String getBudget() {
        return budget;
    }

    /**
     *
     * @param budget
     * The budget
     */
    public void setBudget(String budget) {
        this.budget = budget;
    }

    /**
     *
     * @return
     * The items
     */
    public List<Item> getItems() {
        return items;
    }


    public float getTotalItemCost(){

        float totalPrice = 0;
        if(items.size() > 0){
            for (Item i : items){
                totalPrice = totalPrice + (Float.parseFloat(i.getQuantity().trim())* Float.parseFloat(i.getPrice().trim()));
            }
        }

        return totalPrice;
    }

    public float getBudgetBalance(){

        Float budget = Float.parseFloat(getBudget());
        Float balance = budget - getTotalItemCost();
        return balance;
    }

    /**
     *
     * @param items
     * The items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.budget);
        dest.writeTypedList(items);
    }

    public ShoppingList() {
    }

    private ShoppingList(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.budget = in.readString();
        in.readTypedList(items, Item.CREATOR);
    }

    public static final Parcelable.Creator<ShoppingList> CREATOR = new Parcelable.Creator<ShoppingList>() {
        public ShoppingList createFromParcel(Parcel source) {
            return new ShoppingList(source);
        }

        public ShoppingList[] newArray(int size) {
            return new ShoppingList[size];
        }
    };
}