package my.app.groceryplanning.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

    private String name;
    private String mainCategory;
    private String mainCategoryId;
    private String subCategory;
    private String price;
    private String quantity;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The mainCategory
     */
    public String getMainCategory() {
        return mainCategory;
    }

    /**
     *
     * @param mainCategory
     * The main_category
     */
    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    /**
     *
     * @return
     * The subCategory
     */
    public String getSubCategory() {
        return subCategory;
    }

    /**
     *
     * @param subCategory
     * The sub_category
     */
    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    /**
     *
     * @return
     * The price
     */
    public String getPrice() {
        return price;
    }

    /**
     *
     * @param price
     * The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(String mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.mainCategory);
        dest.writeString(this.subCategory);
        dest.writeString(this.price);
        dest.writeString(this.quantity);
        dest.writeString(this.mainCategoryId);
    }

    public Item() {
    }

    private Item(Parcel in) {
        this.name = in.readString();
        this.mainCategory = in.readString();
        this.subCategory = in.readString();
        this.price = in.readString();
        this.quantity = in.readString();
        this.mainCategoryId = in.readString();
    }

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        public Item createFromParcel(Parcel source) {
            return new Item(source);
        }

        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
}