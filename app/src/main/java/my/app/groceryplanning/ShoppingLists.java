package my.app.groceryplanning;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import my.app.groceryplanning.adapter.ShoppingListAdapter;
import my.app.groceryplanning.database.DbShoppingList;


public class ShoppingLists extends BaseActivity {

    public ListView lvShopping;
    public Button btnNewList;
    public ArrayList<String> sShopping;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);

        lvShopping = (ListView)findViewById(R.id.lvShopping);
        btnNewList = (Button)findViewById(R.id.btnNewList);

        btnNewList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShoppingLists.this, CreateList.class);
                startActivity(i);
            }
        });

        List<DbShoppingList> shoppingLists = DbShoppingList.getAll();
        if(shoppingLists.size() > 0){

            sShopping = new ArrayList<String>();
            for (DbShoppingList list : shoppingLists) {
                System.out.print(list.listName);
                sShopping.add(list.listName);
            }

            ShoppingListAdapter slAdapter = new ShoppingListAdapter(this,0);
            slAdapter.addAll(shoppingLists);

            lvShopping.setAdapter(slAdapter);
        }

    }
}
